# Copyright 2016 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=simtr pn=The-Powder-Toy tag=v${PV} ] \
    scons \
    flag-o-matic \
    lua [ whitelist='5.1 5.2' with_opt=true ] \
    freedesktop-desktop \
    freedesktop-mime \
    gtk-icon-cache

SUMMARY="Falling sand physics sandbox, simulating air pressure, velocity, heat, and other interactions"
SLOT="0"
LICENCES="GPL-3"

# OpenGL isn't recommended by upstream at this point in time

MYOPTIONS="
    platform:
        amd64
"
# opengl

PLATFORMS="~amd64"

DEPENDENCIES="
    build+run:
        sci-libs/fftw
        media-libs/SDL:0
        x11-libs/libX11
        lua? ( dev-lang/LuaJIT )
"
#        opengl? (
#            media-libs/glew
#            x11-dri/mesa
#            x11-dri/glu
#        )

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/953687a5c2529a85516c545b17d5f8603127504b.patch
)

src_prepare() {
    default

    edo sed \
        -e "s|pkg-config|${PKG_CONFIG}|" \
        -i SConscript
}

# f scons
src_configure() {
    :
}

src_compile() {
    escons  \
        --jobs ${EXJOBS:-1} \
        CFLAGS="${CFLAGS}" \
        CXXFLAGS="${CXXFLAGS}" \
        LDFLAGS="${LDFLAGS}" \
        --lin \
        --release \
        --tool=$(exhost --tool-prefix) \
        --output="the-powder-toy" \
        --save-version=$(ever range 1) \
        --minor-version=$(ever range 2) \
        --build-number=$(ever range 3) \
        $(option platform:amd64 --64bit) \
        $(option lua --luajit --nolua) \
        $(option lua_abis:5.2 --lua52) \
        $(if [[ "$(get-flag -mtune)" == native ]];then
            echo "--native"
        else
            echo "--no-sse"
        fi)
#        $(option opengl --opengl) \
#        $(option opengl --opengl-renderer) \
}

src_install() {
    newbin build/the-powder-toy powder

    insinto /usr/share/applications
    doins resources/powder.desktop

    for size in 16 24 32 48 256 ; do
        insinto /usr/share/icons/hicolor/${size}x${size}/apps
        newins resources/icon/powder-${size}.png powder.png
    done

    insinto /usr/share/metainfo
    doins resources/powder.appdata.xml

    insinto /usr/share/mime/packages
    doins resources/powdertoy-save.xml

    emagicdocs
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

